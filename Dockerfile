from python:3.7-slim

ADD ./requirements.txt .

RUN pip install -r requirements.txt

ADD . /app

WORKDIR /app

