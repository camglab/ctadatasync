
while true
do
    python app.py
    ts=`date +%s`

    ats=`expr $ts + 30`

    wakeat=`expr $ats - $ats % 60 + 60 - 30`

    rest=`expr $wakeat - $ts`

    echo `date -d @$ts` '=>' `date -d @$wakeat` "| sleep $rest"  

    sleep $rest

done