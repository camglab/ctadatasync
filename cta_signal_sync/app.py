import redis
from influxdb import InfluxDBClient
import os
import logging
import simplejson as json
from datetime import datetime
import traceback
import sys


logging.basicConfig(
    datefmt="%Y-%m-%dT%H:%M:%S",
    format="%(asctime)s | %(message)s",
    level=logging.INFO
)


def init_influx_from_env():
    return InfluxDBClient(
        os.environ.get("INFLUXDB_HOST", "localhost"),
        int(os.environ.get("INFLUXDB_PORT", "8086")),
        os.environ.get("INFLUXDB_USERNAME", "root"),
        os.environ.get("INFLUXDB_PASSWORD", "root"),
        os.environ.get("INFLUXDB_DATABASE", "db_cta_data")
    )


def init_redis_from_env():
    return redis.Redis.from_url(
        os.environ.get("RDS_URL")
    )


def get_signal_names_from_env():
    var  = os.environ.get("SIGNAL_NAMES", "")
    if var:
        names = var.split(",")
    else:
        names = []

    return names


def get_table_name_from_env():
    return os.environ.get("INFLUXDB_MEASUREMENT", "CTA_SIGNAL")


def clear():
    influx_client = init_influx_from_env()
    measurement = get_table_name_from_env()
    influx_client.drop_measurement(measurement)
    logging.info(f"[clear measurement] {measurement}")


def sync():
    signal_names = get_signal_names_from_env()
    logging.info(f"[signal names] {signal_names}")

    if not signal_names:
        return
    influx_client = init_influx_from_env()
    redis_client = init_redis_from_env()

    measurement = get_table_name_from_env()

    keys = []
    pipline = redis_client.pipeline()
    for signal_name in signal_names: 
        sync_name = f"strategy_sync:{signal_name}" if not signal_name.startswith("strategy_sync:") else signal_name
        pipline.hget(sync_name, "params")
        pipline.hget(sync_name, "signal")
        keys.append((signal_name, "timestamp"))
        keys.append((signal_name, "signal"))
    result = pipline.execute()
    dct = {}
    for items, value in zip(keys, result):
        if value:
            dct.setdefault(items[0], {})[items[1]] = value.decode("utf-8")
        else:
            logging.warning(f"[signal missing] {items}")
    
    points = []

    for sname, svalues in dct.items():
        try:
            ts = int(svalues["timestamp"])
            signal_value = json.loads(svalues["signal"])
        except:
            logging.error(f"[decode signal failed] [{sname} {svalues}]\n{traceback.format_exc()}")
            continue
        for tag, target in signal_value.items():
            logging.info(f"[signal value] {datetime.fromtimestamp(ts)} {sname}.{tag} {target} ")
            points.append({
                "time": ts,
                "measurement": measurement,
                "tags": {
                    "signal_name": sname,
                    "order_tag": tag
                },
                "fields": {key: float(value) for key, value in target.items()}
            })
    
    if len(points):
        r = influx_client.write_points(points, time_precision="s")
        logging.info(f"[write influx] {r}")
    else:
        logging.info(f"[no data to write]")


def main():
    commands = sys.argv[1:]
    logging.info(f"[commands] {commands}")
    if not commands:
        sync()
    else:
        for command in commands:
            if command == "clear":
                clear()



if __name__ == "__main__":
    main()