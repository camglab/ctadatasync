from influxdb import InfluxDBClient
import time
import traceback
import os
import sys



def init_influx_from_env():
    return InfluxDBClient(
        os.environ.get("INFLUXDB_HOST", "localhost"),
        int(os.environ.get("INFLUXDB_PORT", "8086")),
        os.environ.get("INFLUXDB_USERNAME", "root"),
        os.environ.get("INFLUXDB_PASSWORD", "root"),
        os.environ.get("INFLUXDB_DATABASE", "db_cta_data")
    )


def transfer():
    client = init_influx_from_env()
    result = client.query("select * from cta_signal")
    points = []
    for doc in result.get_points():
        points.append({
            "measurement": "CTA_SIGNAL",
            "tags": {
                "signal_name": doc.pop("signal_1"),
                "order_tag": doc.pop("order_tag")
            },
            "fields": doc,
            "time": doc.pop("time")
        })
        for key, value in list(doc.items()):
            if value is None:
                continue
            doc[key] = float(value)
        if len(points) % 200 == 0:
            print(len(points), points[-1])


    print(len(points))
    client.write_points(points)


def test_partial_write():
    measurement = "test_table"
    client = init_influx_from_env()
    ts = int(time.time())

    points = [
         {
            "measurement": measurement,
            "tags": {"signal_name": "test"},
            "fields": {"value": 0},
            "time": ts
        },
        {
            "measurement": measurement,
            "tags": {"signal_name": "test"},
            "fields": {"value": 1},
            "time": 0
        },
        {
            "measurement": measurement,
            "tags": {"signal_name": "test"},
            "fields": {"value": 2},
            "time": ts + 1
        }
    ]

    try:
        r = client.write_points(points, time_precision="s")
    except:
        traceback.print_exc()

    result = client.query("select * from test_table")
    print(result.raw)

    client.drop_measurement(measurement)


def main():
    test_partial_write()


if __name__ == "__main__":
    main()